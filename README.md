# miniproject

## Introduction

Miniproject is a simple web-based project manager.

## Installation sources

Clone repository:

```shell
$ git clone git@gitlab.fabnet.eu:fab/miniproject.git
$ cd miniproject
```

Create python's virtual environment and activate it:

```shell
$ python3 -m venv env
$ source env/bin/activate
```

Update pip and setuptools:

```shell
env $ pip install pip --upgrade
env $ pip install setuptools --upgrade
```

Install required modules:

```shell
env $ pip install -r requirements.txt
```

Generate the application's secret:

```python
>>> import os
>>> os.urandom(24)
b'\x9f<\xb5\xf5\x16t6\xc8...................'
```

Setup copy this secret into `app/config.py`:

```python
     13 # secret is generated by os.urandom(24)
     14 SECRET = b'\x9f<\xb5\xf5\x16t6\xc8...................'
```

Setup database (make sure your virtual environment is still activated):

```shell
env $ python app/build_database.py
```

A default user is created: `admin`. Its password is: `admin`. Its's **strongly recommended** to delete this default user after creating your own ones.

Deactivate your virtual environment:

```shell
env $ deactivate
```

## Run the application as an Apache application WSGI module

At first, make sure Apache is installed and WSGI module for python3 is enabled.

Then setup a site configuration file.

Assuming that:
  * your user and your group are `john`
  * you were in your home directory when cloning the git repo
  * you want to run the application as user `john` and group `john`
  * you want to access the app through the URL http://your.own.domain/miniproject

Your site configuration file would look like this one :

```conf
<VirtualHost *:80>

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        WSGIDaemonProcess miniproject user=john group=john threads=5 python-home=/home/john/miniproject/env
        WSGIProcessGroup miniproject
        WSGIApplicationGroup %{GLOBAL}

        WSGIScriptAlias /miniproject /home/john/miniproject/app/miniproject.wsgi
        WSGIPassAuthorization On

        <Directory /home/john/miniproject/app>
                Require all granted
        </Directory>
</VirtualHost>
```

> **NB: This is a simple example. It is not safe enough because there is no TLS encryption (HTTPS). You'd never deploy an application with an authentication mecanism without encryption.**

Lastly, restart Apache.

## Run the application as a development application

Optionally, you can make the db engine log SQL statements in `app/config.py`:

```python
      9 engine = create_engine('sqlite:///'+ DBFILE , echo=True)
```

Activate your virtual environment:

```shell
$ source env/bin/activate
```

Run the application:

```shell
$ python app/server.py
```

## Entity relationship diagram

![alt text](misc/entity_relationship_diagram.png "Entity Relationship Diagram")

## Users endpoints

| Action | HTTP Verb | URL Path | Description |
| --- | --- | --- | --- |
| Create | POST | /api/users | Defines a unique URL to create a new user |
| Read | GET | /api/users | Defines a unique URL to read a collection of users |
| Read | GET | /api/users/0 | Defines a unique URL to read a particular user in the users collection |
| Update | PUT | /api/users/0 | Defines a unique URL to update an existing user |
| Delete | DELETE | /api/users/0 | Defines a unique URL to delete an existing user |

## Projects endpoints

| Action | HTTP Verb | URL Path | Description |
| --- | --- | --- | --- |
| Create | POST | /api/projects | Defines a unique URL to create a new project |
| Read | GET | /api/projects | Defines a unique URL to read a collection of projects |
| Read | GET | /api/projects/0 | Defines a unique URL to read a particular project in the projects collection |
| Update | PUT | /api/projects/0 | Defines a unique URL to update an existing project |
| Delete | DELETE | /api/projects/0 | Defines a unique URL to delete an existing project |

## Actors endpoints

| Action | HTTP Verb | URL Path | Description |
| --- | --- | --- | --- |
| Create | POST | /api/actors | Defines a unique URL to create a new actor |
| Read | GET | /api/actors | Defines a unique URL to read a collection of actors |
| Read | GET | /api/actors/0 | Defines a unique URL to read a particular actor in the actors collection |
| Delete | DELETE | /api/actors/0 | Defines a unique URL to delete an existing actor |

## Tasks endpoints

| Action | HTTP Verb | URL Path | Description |
| --- | --- | --- | --- |
| Create | POST | /api/tasks | Defines a unique URL to create a new task |
| Read | GET | /api/tasks | Defines a unique URL to read a collection of tasks |
| Read | GET | /api/tasks/0 | Defines a unique URL to read a particular task in the tasks collection |
| Update | PUT | /api/tasks/0 | Defines a unique URL to update an existing task |
| Delete | DELETE | /api/tasks/0 | Defines a unique URL to delete an existing task |

## Login endpoint

| Action | HTTP Verb | URL Path | Description |
| --- | --- | --- | --- |
| Create | POST | /api/login | Defines a unique URL to create a token |
