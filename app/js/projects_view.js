miniprojectApp.controller("projectsViewCtrl", [ '$scope', 'miniprojectFact', '$location', '$routeParams', function ($scope, miniprojectFact, $location, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.users = [];
  $scope.page = "projects";
  $scope.currentProject = {};

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.projects = miniprojectFact.getProjects();
    $scope.projects.forEach(function(curProject) {
      if (curProject.projectid == $routeParams.projectid) {
        $scope.currentProject = curProject
      }
    });
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.serializeActors = function(actors) {
    ret = "";
    if (!(actors === undefined)) {
      actors.forEach(function(curActor) {
        ret += curActor.fname + ", "
      });
    }
    return ret.replace(/, $/gm,'');;
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);