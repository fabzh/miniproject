var miniprojectApp = angular.module("miniproject", ['ngRoute', 'miniprojectControllers']);

miniprojectApp.run(function($window, miniprojectFact) {
  var windowElement = angular.element($window);
  windowElement.on('beforeunload', function (event) {
    miniprojectFact.requestUpdate();
    miniprojectFact.reload();
    event.preventDefault();
  });
});

miniprojectApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'loginCtrl'
      }).
      when('/logout', {
        templateUrl: 'partials/logout.html',
        controller: 'logoutCtrl'
      }).
      when('/projects', {
        templateUrl: 'partials/projects_list.html',
        controller: 'projectsListCtrl'
      }).
      when('/projects/create', {
        templateUrl: 'partials/projects_create.html',
        controller: 'projectsCreateCtrl'
      }).
      when('/projects/delete/:projectid', {
        templateUrl: 'partials/projects_delete.html',
        controller: 'projectsDeleteCtrl'
      }).
      when('/projects/edit/:projectid', {
        templateUrl: 'partials/projects_edit.html',
        controller: 'projectsEditCtrl'
      }).
      when('/projects/view/:projectid', {
        templateUrl: 'partials/projects_view.html',
        controller: 'projectsViewCtrl'
      }).
      when('/tasks/delete/:taskid', {
        templateUrl: 'partials/tasks_delete.html',
        controller: 'tasksDeleteCtrl'
      }).
      when('/tasks/view/:taskid', {
        templateUrl: 'partials/tasks_view.html',
        controller: 'tasksViewCtrl'
      }).
      when('/tasks/create/:projectid', {
        templateUrl: 'partials/tasks_create.html',
        controller: 'tasksCreateCtrl'
      }).
      when('/tasks/edit/:taskid', {
        templateUrl: 'partials/tasks_edit.html',
        controller: 'tasksEditCtrl'
      }).
      when('/tasks', {
        templateUrl: 'partials/tasks_list.html',
        controller: 'tasksListCtrl'
      }).
      when('/users', {
        templateUrl: 'partials/users_list.html',
        controller: 'usersListCtrl'
      }).
      when('/users/view/:userid', {
        templateUrl: 'partials/users_view.html',
        controller: 'usersViewCtrl'
      }).
      when('/users/create', {
        templateUrl: 'partials/users_create.html',
        controller: 'usersCreateCtrl'
      }).
      when('/users/edit/:userid', {
        templateUrl: 'partials/users_edit.html',
        controller: 'usersEditCtrl'
      }).
      when('/users/delete/:userid', {
        templateUrl: 'partials/users_delete.html',
        controller: 'usersDeleteCtrl'
      }).
      otherwise({
        redirectTo: '/logout'
      });
  }]);

var miniprojectControllers = angular.module('miniprojectControllers', []);

miniprojectApp.factory('miniprojectFact', ['$http', '$location', '$route', function($http, $location, $route){

  var user = {};
  var users = [];
  var projects = [];
  var actors = [];
  var tasks = [];
  var projectFilters = {
    search: "",
    myProjectsOnly: false,
    toBeAffected: true,
    inProgress: true,
    frozen: true,
    cancelled: false,
    closed: false
  };
  var taskFilters = {
    search: "",
    myTasksOnly: false,
    inProgress: true,
    frozen: true,
    cancelled: false,
    closed: false
  };
  var userFilters = {
    search: ""
  };
  var projectStates = [
    "To be affected",
    "In progress",
    "Frozen",
    "Cancelled",
    "Closed"
  ];
  var taskStates = [
    "In progress",
    "Frozen",
    "Cancelled",
    "Closed"
  ];
  var history = [];
  var token = "";
  var authorization = "";
  var tokenPayload = {};

  function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  };

  function updateData() {
    ready = false;
    users = [];
    projects = [];
    actors = [];
    tasks = [];
    // GET /api/users
    $http({
      method: "GET",
      url: "api/users"
    }).then(
      // GET /api/users succeeded
      function(getUsersResponse) {
        users = getUsersResponse.data;
        users.forEach(curUser => {
          curUser.nbProjAsOwner = 0;
          curUser.nbProjAsActor = 0;
        });
        // GET /api/projects
        $http({
          method: "GET",
          url: "api/projects"
        }).then(
          // GET /api/projects succeeded
          function(getProjectsResponse) {
            projects = getProjectsResponse.data;
            // GET /api/actors
            $http({
              method: "GET",
              url: "api/actors"
            }).then(
              // GET /api/actors succeeded
              function(getActorsResponse) {
                actors = getActorsResponse.data;
                // GET /api/tasks
                $http({
                  method: "GET",
                  url: "api/tasks"
                }).then(
                  // GET /api/tasks succeeded
                  function(getTasksResponse) {
                    tasks = getTasksResponse.data;
                    tasks.forEach(function(curTask) {
                      curTask.project = {};
                    });
                    projects.forEach(function(curProject) {
                      curProject.user = {};
                      curProject.actors = [];
                      curProject.tasks = [];
                      users.forEach(function(curUser) {
                        if (curProject.user_id == curUser.userid) {
                          curProject.user = curUser;
                          curUser.nbProjAsOwner += 1;
                        }
                      });
                      actors.forEach(function(curActor){
                        users.forEach(function(curUser) {
                          if ((curActor.project_id == curProject.projectid)
                            && (curActor.user_id == curUser.userid)) {
                              curProject.actors.push(curUser);
                            }
                        });
                      });
                      tasks.forEach(function(curTask) {
                        if (curTask.project_id == curProject.projectid) {
                          curProject.tasks.push(curTask);
                          curTask.project = curProject;
                        }
                      });
                    });
                    actors.forEach(curActor => {
                      users.forEach(curUser => {
                        if (curActor.user_id == curUser.userid) {
                          curUser.nbProjAsActor += 1;
                        }
                      });
                    });
                    users.forEach(curUser => {
                      if (curUser.userid == tokenPayload.sub) {
                        user = curUser;
                      }
                    });
                    ready = true;
                  },
                  // GET /api/tasks failed
                  function(getTasksResponse) {
                    if (getTasksResponse.status == 403) {
                      $location.path('/logout');
                    }
                  }
                );
              },
              // GET /api/actors failed
              function(getActorsResponse) {
                if (getActorsResponse.status == 403) {
                  $location.path('/logout');
                }
              }
            );
          },
          // GET /api/projects failed
          function(getProjectsResponse) {
            if (getProjectsResponse.status == 403) {
              $location.path('/logout');
            }
          }
        );
      },
      // GET /api/users failed
      function(getUsersResponse) {
        if (getUsersResponse.status == 403) {
          $location.path('/logout');
        }
      }
    );
  }

  var ready = true;

  return {
    addToHistory: function(path) {history.push(path)},
    getActors: function() {return actors},
    getAuthorization: function() {return authorization;},
    getProjectFilters: function() {return projectFilters},
    getProjects: function() {return projects},
    getScopes: function() { return tokenPayload.scope.split(" ")},
    getTaskFilters: function() {return taskFilters},
    getTasks: function() {return tasks},
    getUser: function() {
      if (token=="") {
        $location.path("/login");
      }
      return user;
    },
    getUserFilters: function() {return userFilters},
    getUsers: function() {return users},
    goBack: function() {
      var location = "/projects";
      while ((location == "/projects") && (history.length > 0)) {
        var curLocation = history.pop();
        if ((/^\/projects\/view/.test(curLocation)) || (/^\/tasks$/.test(curLocation))) {
          location = curLocation;
        }
      }
      $location.path(location);
    },
    isReady: function() {return ready;},
    logout: function() {
      user = {};
      users = [];
      projects = [];
      actors = [];
      tasks = [];
      token = "";
      tokenPayload = {};
      authorization = "";
      projectFilters = {
        search: "",
        myProjectsOnly: false,
        toBeAffected: true,
        inProgress: true,
        frozen: true,
        cancelled: false,
        closed: false
      };
      taskFilters = {
        search: "",
        myTasksOnly: false,
        inProgress: true,
        frozen: true,
        cancelled: false,
        closed: false
      };
      userFilters = {
        search: ""
      };
      history = [];
      $location.path("/login");
    },
    projectStates: function() {return projectStates;},
    reload: function() {$route.reload();},
    requestUpdate: function() {if (ready) {updateData()}},
    setProjects: function(newProjects) {projects = newProjects},
    setToken: function(newToken) {
      token = newToken;
      tokenPayload = parseJwt(token);
      authorization = "Bearer " + token;
      $http.defaults.headers.common.Authorization = authorization;
    },
    taskStates: function() {return taskStates;},
  }
}]);