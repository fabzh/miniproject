miniprojectApp.controller("tasksListCtrl", [ '$scope', 'miniprojectFact', '$location', function ($scope, miniprojectFact, $location) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.users = [];
  $scope.tasks = [];
  $scope.page = "tasks";
  $scope.filters = {};

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.tasks = miniprojectFact.getTasks();
    $scope.filters = miniprojectFact.getTaskFilters();
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.tasksFilter = function(task) {
    var ret = true;
    var userIsOwner = false;
    var userIsActor = false;
    if ($scope.filters.myTasksOnly) {
      userIsOwner = (task.project.user_id == $scope.user.userid);
      task.project.actors.forEach(function(curActor) {
        if (curActor.userid == $scope.user.userid) {
          userIsActor = true;
        }
      });
      ret = (ret && (userIsOwner || userIsActor));
    }
    if (task.state == 'In progress') {
      ret = (ret && $scope.filters.inProgress);
    }
    else if (task.state == 'Frozen') {
      ret = (ret && $scope.filters.frozen);
    }
    else if (task.state == 'Cancelled') {
      ret = (ret && $scope.filters.cancelled);
    }
    else if (task.state == 'Closed') {
      ret = (ret && $scope.filters.closed);
    }
    return ret;
  };

  $scope.tasksSearch = function(task) {
    var ret = false;
    var patt = RegExp($scope.filters.search);
    ret = (ret || patt.test(task.description));
    ret = (ret || patt.test(task.project.name));
    return ret;
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);