miniprojectApp.controller("usersCreateCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', function ($scope, miniprojectFact, $location, $http) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.users = [];
  $scope.page = "users";
  $scope.newUser = {
    fname: "",
    lname: "",
    login: "",
    role: "user",
    password: "",
    confirmation: "",
  };

  $scope.createUser = function() {
    // Prepare newUser object
    var newUser = {
      login: $scope.newUser.login,
      password: $scope.newUser.password,
      role: $scope.newUser.role
    };
    if ($scope.newUser.fname != "") {
      newUser.fname = $scope.newUser.fname;
    }
    if ($scope.newUser.lname != "") {
      newUser.lname = $scope.newUser.lname;
    }
    // POST /api/users
    $http({
      method: "POST",
      url: "api/users",
      data: newUser
    }).then(
      // POST /api/users succeeded
      function(createUsersResponse) {
        miniprojectFact.requestUpdate();
        $scope.goTo('/users');
      },
      // POST /api/users failed
      function(createUsersResponse) {
        if (createUsersResponse.status == 403) {
          $location.path('/logout');
        }
        else {
          $scope.error = createUsersResponse.data.detail;
        }
      }
    );
  }

  $scope.formIsInvalid = function() {
    var ret = false;
    if ($scope.newUser.fname == "") {
      ret = true
    }
    if ($scope.newUser.password == "") {
      ret = true
    }
    if ($scope.newUser.login == "") {
      ret = true
    }
    if ($scope.newUser.password != $scope.newUser.confirmation) {
      ret = true
    }
    return ret;
  };

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);