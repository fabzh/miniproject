miniprojectApp.controller("usersListCtrl", [ '$scope', 'miniprojectFact', '$location', function ($scope, miniprojectFact, $location) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.users = [];
  $scope.page = "users";
  $scope.filters = {};

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.filters = miniprojectFact.getUserFilters();
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);