miniprojectApp.controller("tasksCreateCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', '$routeParams', function ($scope, miniprojectFact, $location, $http, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.projectid = $routeParams.projectid;
  $scope.users = [];
  $scope.page = "tasks";
  $scope.currentProject = {};
  $scope.newTask = {
    description: "",
    state: "In progress"
  };
  $scope.taskStates = miniprojectFact.taskStates();

  $scope.createTask = function() {
    // Prepare newTask object
    var newTask = {
      project_id: $scope.currentProject.projectid,
    };
    if ($scope.newTask.description != "") {
      newTask.description = $scope.newTask.description;
    }
    if ($scope.newTask.state != "") {
      newTask.state = $scope.newTask.state;
    }
    // POST /api/tasks
    $http({
      method: "POST",
      url: "api/tasks",
      data: newTask
    }).then(
      // POST /api/tasks succeeded
      function(createTasksResponse) {
        miniprojectFact.requestUpdate();
        miniprojectFact.goBack();
      },
      // POST /api/tasks failed
      function(createTasksResponse) {
        if (createTasksResponse.status == 403) {
          $location.path('/logout');
        }
        else {
          $scope.error = createTasksResponse.data.detail;
        }
      }
    );
  }

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.projects = miniprojectFact.getProjects();
    if ($routeParams.projectid > -1) {
      $scope.projects.forEach(curProject => {
        if (curProject.projectid == $routeParams.projectid) {
          $scope.currentProject = curProject;
        }
      });
    };
  };

  $scope.goBack = function() {
    miniprojectFact.goBack();
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);