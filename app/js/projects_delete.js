miniprojectApp.controller("projectsDeleteCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', '$routeParams', function ($scope, miniprojectFact, $location, $http, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.users = [];
  $scope.page = "projects";
  $scope.projectid = $routeParams.projectid;
  $scope.secret = Math.floor(Math.random() * 10000).toString();
  while ($scope.secret.length < 4 ) {
    $scope.secret = "0" + $scope.secret
  }
  $scope.confirmation = "";

  $scope.deleteProject = function() {
    // DELETE /api/projects/:projectid
    $http({
      method: "DELETE",
      url: "api/projects/" + $scope.projectid
    }).then(
      function(deleteProjectsResponse) {
        miniprojectFact.requestUpdate();
        $scope.goTo("/projects");
      },
      function(deleteProjectsResponse) {
        if (deleteProjectsResponse == 403) {
          $location.path('/logout');
        }
        else {
          miniprojectFact.requestUpdate();
          $scope.goTo("/projects");
        }
      }
    );
  }

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);