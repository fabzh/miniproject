miniprojectApp.controller("loginCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', function ($scope, miniprojectFact, $location, $http) {

    $scope.ready = miniprojectFact.isReady;
    miniprojectFact.addToHistory($location.path());
    $scope.login = "";
    $scope.password = "";
    $scope.error = "";

    $scope.createToken = function() {
      $http({
        method: "post",
        url: "api/login",
        data: {
          login: $scope.login,
          password: $scope.password
        }
      }).then(
        function(data) {
          miniprojectFact.setToken(data.data);
          $("body").css("background-color", "white");
          $location.path( "/projects" );
        },
        function(data) {
          $scope.error = data.data.detail;
        }
      );
    };
    
    // Vérification toutes les 100ms que la factory est ready
    $scope.intervalID = setInterval(function(){
      if (miniprojectFact.isReady()) {
        $scope.$apply(function() {$scope.ready = true;});
        clearInterval($scope.intervalID);
      }
    }, 100);
    
    $("body").css("background-color", "grey");
  }]);