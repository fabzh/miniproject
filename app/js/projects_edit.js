miniprojectApp.controller("projectsEditCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', '$routeParams', function ($scope, miniprojectFact, $location, $http, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.users = [];
  $scope.page = "projects";
  // object bound to edit view
  $scope.editedProject = {
    projectid: -1,
    name: "",
    actors : [],
    description: "",
    state: "In progress",
    user: {}
  };
  $scope.projectStates = miniprojectFact.projectStates();

  $scope.actorsFilter = function(user) {
    var ret = true;
    if (($scope.editedProject.user) && ($scope.editedProject.user.login == user.login)) {
      ret = false;
    }
    return ret;
  };

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.projects = miniprojectFact.getProjects();
    $scope.projects.forEach(function(curProject) {
      if (curProject.projectid == $routeParams.projectid) {
        $scope.editedProject = {
          projectid: curProject.projectid,
          name: curProject.name,
          actors : curProject.actors,
          description: curProject.description,
          state: curProject.state,
          user: curProject.user
        };
      }
    });
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.ownersFilter = function(user) {
    var ret = true;
    $scope.editedProject.actors.forEach(function(curActor) {
      if (curActor.login == user.login) {
        ret = false;
      }
    });
    return ret;
  };

  $scope.saveProject = function() {
    // Prepare newProject object
    var editedProject = {
      name: $scope.editedProject.name,
      description: $scope.editedProject.description,
      state: $scope.editedProject.state,
      user_id: -1
    };
    if ($scope.editedProject.user) {
      editedProject.user_id = $scope.editedProject.user.userid;
    }
    // PUT /api/projects/:projectid
    $http({
      method: "PUT",
      url: "api/projects/" + $routeParams.projectid,
      data: editedProject
    }).then(
      // PUT /api/projects/:projectid succeeded
      function(createProjectsResponse) {
        // Prepare editedActors object
        var editedActors = {
          project_id: Number($routeParams.projectid),
          user_ids: []
        };
        $scope.editedProject.actors.forEach(function(curActor) {
          editedActors.user_ids.push(curActor.userid);
        });
        // POST /api/actors
        $http({
          method: "POST",
          url: "api/actors",
          data: editedActors
        }).then(
          // POST /api/actors succeeded
          function(postActorsResponse) {
            miniprojectFact.requestUpdate();
            $scope.goTo('/projects/view/' + $scope.editedProject.projectid);
          },
          // POST /api/actors failed
          function(postActorsResponse) {
            if (postActorsResponse.status == 403) {
              $location.path('.logout');
            }
            else {
              miniprojectFact.requestUpdate();
              $scope.goTo('/projects/view/' + $scope.editedProject.projectid);
            }
          }
        );
      },
      // PUT /api/projects/:projectid failed
      function(createProjectsResponse) {
        if (createProjectsResponse.status == 403) {
          $location.path('/logout');
        }
        else {
          $scope.error = createProjectsResponse.data.detail;
        }
      }
    );
  }

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);