miniprojectApp.controller("tasksViewCtrl", [ '$scope', 'miniprojectFact', '$location', '$routeParams', function ($scope, miniprojectFact, $location, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.users = [];
  $scope.tasks = [];
  $scope.page = "tasks";
  $scope.currentProject = {};
  $scope.currentTask = {};

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.projects = miniprojectFact.getProjects();
    $scope.tasks = miniprojectFact.getTasks();
    $scope.tasks.forEach(function(curTask) {
      if (curTask.taskid == $routeParams.taskid) {
        $scope.currentTask = curTask;
      }
    });
    $scope.projects.forEach(function(curProject) {
      if (curProject.projectid == $scope.currentTask.project_id) {
        $scope.currentProject = curProject;
      }
    });
  };

  $scope.goBack = function() {
    miniprojectFact.goBack();
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);