miniprojectApp.controller("tasksEditCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', '$routeParams', function ($scope, miniprojectFact, $location, $http, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.users = [];
  $scope.tasks = [];
  $scope.page = "tasks";
  $scope.currentProject = {};
  $scope.editedTask = {};
  $scope.taskStates = miniprojectFact.taskStates();

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.projects = miniprojectFact.getProjects();
    $scope.tasks = miniprojectFact.getTasks();
    $scope.tasks.forEach(curTask => {
      if (curTask.taskid == $routeParams.taskid) {
        $scope.editedTask = curTask;
      }
    });
    $scope.projects.forEach(curProject => {
      if (curProject.projectid == $scope.editedTask.project_id) {
        $scope.currentProject = curProject;
      }
    });
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.saveTask = function() {
    // Prepare editedTask object
    var editedTask = {
      description: $scope.editedTask.description,
      state: $scope.editedTask.state
    };
    // PUT /api/tasks/:taskid
    $http({
      method: "PUT",
      url: "api/tasks/" + $routeParams.taskid,
      data: editedTask
    }).then(
      // PUT /api/tasks/:taskid succeeded
      function(createTasksResponse) {
        miniprojectFact.requestUpdate();
        $scope.goTo('/tasks/view/' + $routeParams.taskid);
      },
      // PUT /api/tasks/:taskid failed
      function(createTasksResponse) {
        if (createTasksResponse.status == 403) {
          $location.path('/logout');
        }
        else {
          $scope.error = createTasksResponse.data.detail;
        }
      }
    );
  }

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);