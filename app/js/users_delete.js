miniprojectApp.controller("usersDeleteCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', '$routeParams', function ($scope, miniprojectFact, $location, $http, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.users = [];
  $scope.page = "users";
  $scope.userid = $routeParams.userid;
  $scope.secret = Math.floor(Math.random() * 10000).toString();
  while ($scope.secret.length < 4 ) {
    $scope.secret = "0" + $scope.secret
  }
  $scope.confirmation = "";

  $scope.deleteUser = function() {
    // DELETE /api/users/:userid
    $http({
      method: "DELETE",
      url: "api/users/" + $scope.userid
    }).then(
      function(deleteUsersResponse) {
        miniprojectFact.requestUpdate();
        $scope.goTo("/users");
      },
      function(deleteUsersResponse) {
        if (deleteUsersResponse.status == 403) {
          $location.path('/logout');
        }
      }
    );
    
  }

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);