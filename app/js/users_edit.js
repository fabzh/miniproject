miniprojectApp.controller("usersEditCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', '$routeParams', function ($scope, miniprojectFact, $location, $http, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.users = [];
  $scope.page = "users";
  $scope.editedUser = {};

  $scope.saveUser = function() {
    // Prepare editedUser object
    var editedUser = {};
    if ($scope.editedUser.fname != "") {
      editedUser.fname = $scope.editedUser.fname;
    }
    if ($scope.editedUser.lname != "") {
      editedUser.lname = $scope.editedUser.lname;
    }
    if ($scope.editedUser.login != "") {
      editedUser.login = $scope.editedUser.login;
    }
    if ($scope.editedUser.password != "") {
      editedUser.password = $scope.editedUser.password;
    }
    if ($scope.editedUser.role != "") {
      editedUser.role = $scope.editedUser.role;
    }
    // PUT /api/users/:userid
    $http({
      method: "PUT",
      url: "api/users/" + $routeParams.userid,
      data: editedUser
    }).then(
      // PUT /api/users/:userid succeeded
      function(editUsersResponse) {
        miniprojectFact.requestUpdate();
        $scope.goTo('/users/view/' + $routeParams.userid);
      },
      // PUT /api/users/:userid failed
      function(editUsersResponse) {
        if (editUsersResponse.status == 403) {
          $location.path('/logout');
        }
        else {
          $scope.error = editUsersResponse.data.detail;
        }
      }
    );
  }

  $scope.formIsInvalid = function() {
    var ret = false;
    if ($scope.editedUser.password != $scope.editedUser.confirmation) {
      ret = true
    }
    return ret;
  };

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.users.forEach(curUser => {
      if (curUser.userid == $routeParams.userid) {        
        $scope.editedUser = {
        fname: curUser.fname,
        lname: curUser.lname,
        login: curUser.login,
        role: curUser.role,
        password: "",
        confirmation: ""
        };
      }
    });
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);