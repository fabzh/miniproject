miniprojectApp.controller("projectsCreateCtrl", [ '$scope', 'miniprojectFact', '$location', '$http', function ($scope, miniprojectFact, $location, $http) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.users = [];
  $scope.page = "projects";
  $scope.newProject = {
    name: "",
    actors : [],
    description: "",
    state: "In progress",
    user: {}
  };
  $scope.projectStates = miniprojectFact.projectStates();
  $scope.taskStates = miniprojectFact.taskStates();

  $scope.actorsFilter = function(user) {
    var ret = true;
    if (($scope.newProject.user) && ($scope.newProject.user.login == user.login)) {
      ret = false;
    }
    return ret;
  };

  $scope.createProject = function() {
    // Prepare newProject object
    var newProject = {
      name: $scope.newProject.name
    };
    if ($scope.newProject.description != "") {
      newProject.description = $scope.newProject.description;
    }
    if ($scope.newProject.state != "") {
      newProject.state = $scope.newProject.state;
    }
    if ($scope.newProject.user) {
      newProject.user_id = $scope.newProject.user.userid;
    }
    // POST /api/projects
    $http({
      method: "POST",
      url: "api/projects",
      data: newProject
    }).then(
      // POST /api/projects succeeded
      function(createProjectsResponse) {
        $scope.newProject.projectid = createProjectsResponse.data.projectid;
        // Prepare newActors object
        var newActors = {
          project_id: createProjectsResponse.data.projectid,
          user_ids: []
        };
        $scope.newProject.actors.forEach(function(curActor) {
          newActors.user_ids.push(curActor.userid);
        });
        // POST /api/actors
        $http({
          method: "POST",
          url: "api/actors",
          data: newActors
        }).then(
          // POST /api/actors succeeded
          function(postActorsResponse) {
            miniprojectFact.requestUpdate();
            $scope.goTo('/projects/view/' + $scope.newProject.projectid);
          },
          // POST /api/actors failed
          function(postActorsResponse) {
            if (postActorsResponse.status == 403) {
              $location.path('/logout');
            }
            else {
              miniprojectFact.requestUpdate();
              $scope.goTo('/projects/view/' + $scope.newProject.projectid);
            }
          }
        );
      },
      // POST /api/projects failed
      function(createProjectsResponse) {
        if (createProjectsResponse.status == 403) {
          $location.path('/logout');
        }
        else {
          $scope.error = createProjectsResponse.data.detail;
        }
      }
    );
  }

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.projects = miniprojectFact.getProjects();
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.ownersFilter = function(user) {
    var ret = true;
    $scope.newProject.actors.forEach(function(curActor) {
      if (curActor.login == user.login) {
        ret = false;
      }
    });
    return ret;
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);