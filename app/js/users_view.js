miniprojectApp.controller("usersViewCtrl", [ '$scope', 'miniprojectFact', '$location', '$routeParams', function ($scope, miniprojectFact, $location, $routeParams) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.user = {};
  $scope.users = [];
  $scope.page = "users";
  $scope.scopes = [];
  $scope.currentUser = {};

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.scopes = miniprojectFact.getScopes();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.users.forEach(function(curUser) {
      if (curUser.userid == $routeParams.userid) {
        $scope.currentUser = curUser;
      }
    });
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.isAdmin = function() {
    return $scope.scopes.includes('admin');
  }

  $scope.isCurrenlyLogged = function() {
    return $scope.user.userid == $scope.currentUser.userid;
  }

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);