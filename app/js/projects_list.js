miniprojectApp.controller("projectsListCtrl", [ '$scope', 'miniprojectFact', '$location', function ($scope, miniprojectFact, $location) {

  $scope.ready = false;
  miniprojectFact.addToHistory($location.path());
  $scope.error = "";
  $scope.projects = [];
  $scope.users = [];
  $scope.actors = [];
  $scope.tasks = [];
  $scope.page = "projects";
  $scope.projectStates = miniprojectFact.projectStates();
  $scope.taskStates = miniprojectFact.taskStates();
  $scope.filters = {};

  $scope.getDataFromFactory = function() {
    // If user is an empty object, factory will redirect to /login
    $scope.user = miniprojectFact.getUser();
    $scope.factoryIsRead = miniprojectFact.isReady();
    $scope.users = miniprojectFact.getUsers();
    $scope.projects = miniprojectFact.getProjects();
    $scope.filters = miniprojectFact.getProjectFilters();
  };

  $scope.goTo = function(path) {
    $location.path(path);
  };

  $scope.projectsFilter = function(project) {
    var ret = true;
    var userIsOwner = false;
    var userIsActor = false;
    if ($scope.filters.myProjectsOnly) {
      userIsOwner = (project.user_id == $scope.user.userid);
      project.actors.forEach(function(curActor) {
        if (curActor.userid == $scope.user.userid) {
          userIsActor = true;
        }
      });
      ret = (ret && (userIsOwner || userIsActor));
    }
    if (project.state == 'To be affected') {
      ret = (ret && $scope.filters.toBeAffected);
    }
    else if (project.state == 'In progress') {
      ret = (ret && $scope.filters.inProgress);
    }
    else if (project.state == 'Frozen') {
      ret = (ret && $scope.filters.frozen);
    }
    else if (project.state == 'Cancelled') {
      ret = (ret && $scope.filters.cancelled);
    }
    else if (project.state == 'Closed') {
      ret = (ret && $scope.filters.closed);
    }
    return ret;
  };

  $scope.projectsSearch = function(project) {
    var ret = false;
    var patt = RegExp($scope.filters.search);
    ret = (ret || patt.test(project.name));
    ret = (ret || patt.test(project.description));
    return ret;
  };

  $scope.serializeActors = function(actors) {
    ret = "";
    actors.forEach(function(curActor) {
      ret += curActor.fname + ", "
    });
    return ret.replace(/, $/gm,'');;
  };

  $scope.waitForUpToDateData = function() {
    $scope.ready = false;
    $scope.getDataFromFactory();
    // If prerequisites are not met...
    if ((!($scope.factoryIsRead)) || (jQuery.isEmptyObject($scope.users))) {
      // ... then request update...
      miniprojectFact.requestUpdate();
      // ... and wait for factory to be ready
      $scope.intervalID = setInterval(function(){
        if (miniprojectFact.isReady()) {
          clearInterval($scope.intervalID);
          $scope.$apply(function() {
            $scope.getDataFromFactory();
            $scope.ready = true;
          });
        }
      }, 100);
    }
    else {
      $scope.ready = true;
    }
  };

  $scope.waitForUpToDateData();

}]);