from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()

class User(Base):
    __tablename__ = 'user'
    userid = Column(Integer, primary_key=True)
    login = Column(String(64), index=True, unique=True)
    fname = Column(String(32))
    lname = Column(String(32))
    password = Column(String(64))
    role = Column(String(16))
    # relationships
    projects = relationship('Project', order_by="Project.projectid", back_populates="user")
    actors = relationship('Actor', order_by="Actor.actorid", back_populates="user")

class Project(Base):
    __tablename__ = 'project'
    projectid = Column(Integer, primary_key=True)
    name = Column(String(256))
    description = Column(String(2048))
    user_id = Column(Integer, ForeignKey('user.userid'), default=None)
    state = Column(String(32))
    ctimestamp = Column(DateTime, default=datetime.utcnow)
    # relationships
    user = relationship('User', back_populates="projects")
    tasks = relationship('Task', order_by="Task.taskid", back_populates="project")
    actors = relationship('Actor', order_by="Actor.actorid", back_populates="project")

class Task(Base):
    __tablename__ = 'task'
    taskid = Column(Integer, primary_key=True)
    description = Column(String(2048))
    project_id = Column(Integer, ForeignKey('project.projectid'))
    state = Column(String(32))
    ctimestamp = Column(DateTime, default=datetime.utcnow)
    mtimestamp = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    # relationships
    project = relationship('Project', back_populates="tasks")

class Actor(Base):
    __tablename__ = 'actor'
    actorid = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.userid'))
    project_id = Column(Integer, ForeignKey('project.projectid'))
    # relationships
    user = relationship('User', back_populates='actors')
    project = relationship('Project', back_populates='actors')

# class UserSchema(ma.Schema):
#     class Meta:
#         model = User
#         sqla_session = session

# class ProjectSchema(ma.Schema):
#     class Meta:
#         model = Project
#         sqla_session = session

# class TaskSchema(ma.Schema):
#     class Meta:
#         model = Task
#         sqla_session = session

# class ActorSchema(ma.Schema):
#     class Meta:
#         model = Actor
#         sqla_session = session