from flask import make_response, abort
from config import Session
from models import Base, User, Project, Task, Actor

def read_all(token_info = None):
    """
    This function responds to a request for /api/tasks
    with the complete lists of tasks
    :param token_info:    dict with token info
    :return:              sorted list of tasks
    """
    session = Session()
    ret = []
    for task in session.query(Task).order_by(Task.taskid):
        ret.append({
            'taskid': task.taskid,
            'description': task.description,
            'project_id': task.project_id,
            'state': task.state,
            'ctimestamp': task.ctimestamp,
            'mtimestamp': task.mtimestamp
        })
    session.close()
    return ret

def create(body, token_info = None):
    """
    This function creates a new task in the tasks structure
    based on the passed in task data
    :param body:          task to create in tasks structure
    :param token_info:    dict with token info
    :return:              201 on success, 406 on task exists
    """
    description = body.get("description", "")
    project_id = body.get("project_id", None)
    state = body.get("state", "In progress")

    if project_id is None:
        abort(
            400,
            "Project id is mandatory.",
        )

    session = Session()
    try:
        session.query(Project).filter(Project.projectid == project_id).one()
    except:
        session.close()
        abort(
            404, "Project with project id {projectid} not found".format(projectid=projectid)
        )

    task = Task(
        description = description,
        project_id = project_id,
        state = state
    )

    session.add(task)

    try:
        session.commit()
    except:
        session.close()
        abort(
            400, "Failed to create task"
        )

    return make_response(
        "{taskid} successfully created".format(taskid=task.taskid), 201
    )

def read_one(taskid, token_info = None):
    """
    This function responds to a request for /api/tasks/{taskid}
    with one matching task from tasks
    :param taskid:        task id of task to find
    :param token_info:    dict with token info
    :return:              task matching task id
    """

    session = Session()
    try:
        task = session.query(Task).filter(Task.taskid == taskid).one()
    except:
        session.close()
        abort(
            404, "Task with task id {taskid} not found".format(taskid=taskid)
        )
    ret = {
        'taskid': task.taskid,
        'description': task.description,
        'project_id': task.project_id,
        'state': task.state,
        'ctimestamp': task.ctimestamp,
        'mtimestamp': task.mtimestamp
    }
    session.close()
    return ret

def update(taskid, body, token_info = None):
    """
    This function updates an existing task in the tasks structure
    :param taskid:        task id of task to update in the tasks structure
    :param body:          task to update
    :param token_info:    dict with token info
    :return:              updated task structure
    """

    session = Session()
    try:
        task = session.query(Task).filter(Task.taskid == taskid).one()
    except:
        session.close()
        abort(
            404, "Task with task id {taskid} not found".format(taskid=taskid)
        )

    task.description = body.get("description", task.description)
    task.state = body.get("state", task.state)

    try:
        session.commit()
    except:
        session.close()
        abort(
            400, "Failed to update task with id {taskid}".format(taskid=taskid)
        )

    ret = {
        'taskid': task.taskid,
        'description': task.description,
        'project_id': task.project_id,
        'state': task.state,
        'ctimestamp': task.ctimestamp,
        'mtimestamp': task.mtimestamp
    }
    session.close()
    return ret

def delete(taskid, token_info = None):
    """
    This function deletes a task from the tasks structure
    :param taskid:        task id of task to delete
    :param token_info:    dict with token info
    :return:              200 on successful delete, 404 if not found
    """

    session = Session()
    try:
        task = session.query(Task).filter(Task.taskid == taskid).one()
    except:
        session.close()
        abort(
            404, "Task with task id {taskid} not found".format(taskid=taskid)
        )

    session.delete(task)

    try:
        session.commit()
    except:
        session.close()
        abort(
            400, "Failed to delete task with id {taskid}".format(taskid=taskid)
        )

    session.commit()
    
    return make_response(
        "{taskid} successfully deleted".format(taskid=taskid), 200
    )