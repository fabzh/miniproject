from flask import make_response, abort
from sqlalchemy import and_
from config import Session
from models import Base, User, Project, Task, Actor

def read_all(token_info = None):
    """
    This function responds to a request for /api/actors
    with the complete lists of actors
    :param token_info:    dict with token info
    :return:              sorted list of actors
    """
    
    session = Session()
    ret = []
    for actor in session.query(Actor).order_by(Actor.actorid):
        ret.append({
            'actorid': actor.actorid,
            'user_id': actor.user_id,
            'project_id': actor.project_id
        })
    session.close()
    return ret

def create(body, token_info = None):
    """
    This function creates new actors in the actors structure
    for a given project based on the passed in actor data.
    Any previous actors for that project are removed.
    :param body:          actors to create in actors structure
    :param token_info:    dict with token info
    :return:              201 on success, 406 on actor exists
    """
    project_id = body.get("project_id", None)
    user_ids = body.get("user_ids", None)

    # user_ids and project_id are mendatory
    if user_ids is None or project_id is None:
        abort(
            400,
            "Project id and user ids are mandatory.",
        )

    session = Session()

    # project_id has to exist
    try:
        session.query(Project).filter(Project.projectid == project_id).one()
    except:
        session.close()
        abort(
            404, "Project with project id {project_id} not found".format(project_id=project_id)
        )
    
    # each user_id has to exist
    for user_id in user_ids:
        try:
            session.query(User).filter(User.userid == user_id).one()
        except:
            session.close()
            abort(
                404, "User with user id {user_id} not found".format(user_id=user_id)
            )

    # Delete previous actors related to this project_id
    for old_actor in session.query(Actor).filter(Actor.project_id == project_id):
        session.delete(old_actor)
    
    # Create as many actors as user_ids
    for user_id in user_ids:
        actor = Actor(user_id = user_id, project_id = project_id)
        session.add(actor)

    try:
        session.commit()
    except:
        session.close()
        abort(
            400,
            "Failed to create actors",
        )

    actorids = [actor.actorid for actor in session.query(Actor).filter(and_( \
        Actor.project_id == project_id, Actor.user_id.in_(user_ids))).order_by(Actor.actorid)]
    session.close()
    return make_response(
        "{actorids} successfully created".format(actorids=",".join([str(a) for a in actorids])), 201
    )

def read_one(actorid, token_info = None):
    """
    This function responds to a request for /api/actors/{actorid}
    with one matching actor from actors
    :param actorid:       actor id of actor to find
    :param token_info:    dict with token info
    :return:              actor matching actor id
    """
    
    session = Session()

    try:
        actor = session.query(Actor).filter(Actor.actorid == actorid).one()
    except:
        session.close()
        abort(
            404, "Actor with actor id {actorid} not found".format(actorid=actorid)
        )

    ret = {
        'actorid': actor.actorid,
        'user_id': actor.user_id,
        'project_id': actor.project_id
    }
    session.close()
    return ret

def delete(actorid, token_info = None):
    """
    This function deletes an actor from the actors structure
    :param actorid:       actor id of actor to delete
    :param token_info:    dict with token info
    :return:              200 on successful delete, 404 if not found
    """

    session = Session()

    try:
        actor = session.query(Actor).filter(Actor.actorid == actorid).one()
    except:
        session.close()
        abort(
            404, "Actor with actor id {actorid} not found".format(actorid=actorid)
        )

    session.delete(actor)

    try:
        session.commit()
    except:
        session.close()
        abort(
            400,
            "Failed to delete actor",
        )

    session.close()

    return make_response(
        "{actorid} successfully deleted".format(actorid=actorid), 200
    )
