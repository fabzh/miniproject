from flask import make_response, abort
from datetime import datetime, timedelta
import jwt
import bcrypt
import os
from config import Session, SECRET
from models import Base, User, Project, Task, Actor

def create_token(body):
    """
    This function creates a new token based on the passed
    in user data
    :param body:    user to authenticate
    :return:        201 on success, 403 wrong login or password
    """
    login = body.get("login", "")
    password = body.get("password", "")

    if not login or not password:
        abort(
            400,
            "Login and password are mandatory.",
        )

    password = password.encode('utf-8')

    session = Session()

    try:
        founduser = session.query(User).filter(User.login == login).one()
        if bcrypt.checkpw(password, founduser.password.encode('utf-8')):
            try:
                payload = {
                    'exp': datetime.utcnow() + timedelta(days=0, seconds=3600),
                    'iat': datetime.utcnow(),
                    'sub': founduser.userid,
                    'scope': founduser.role
                }
                session.close()
                return jwt.encode(
                    payload,
                    SECRET,
                    algorithm='HS256'
                ).decode('utf-8')
            except Exception as e:
                session.close()
                return e
    except:
        session.close()
        abort(
            403,
            "Wrong login or password",
        )
        
def token_info(token):
    try:
        return jwt.decode(token, SECRET, algorithms='HS256')
    except:
        abort(
            403,
            "Token expired/invalid",
        )