from flask import make_response, abort
import bcrypt
from config import Session
from models import Base, User, Project, Task, Actor

# Create a handler for our read (GET) users
def read_all(token_info = None):
    """
    This function responds to a request for /api/users
    with the complete lists of users
    :param token_info:    dict with token info
    :return:              sorted list of users
    """
    # Create the list of users from our data
    session = Session()
    ret = []
    for user in session.query(User).order_by(User.userid):
        ret.append({
            "userid": user.userid,
            "login": user.login,
            "fname": user.fname,
            "lname": user.lname,
            "role": user.role,
        })
    session.close()
    return ret

def create(body, token_info = None):
    """
    This function creates a new user in the users structure
    based on the passed in user data
    :param body:          user to create in users structure
    :param token_info:    dict with token info
    :return:              201 on success, 406 on person exists
    """
    
    new_user = User(
        login = body.get("login", ""),
        fname = body.get("fname", ""),
        lname = body.get("lname", ""),
        password = bcrypt.hashpw(body.get("password", "").encode('utf-8'), bcrypt.gensalt(14)).decode('utf-8'),
        role = body.get("role", "user")
    )

    if not new_user.login or not new_user.password or not new_user.fname:
        abort(
            400,
            "Login, first name and password are mandatory.",
        )

    session = Session()
    try:
        session.add(new_user)
        session.commit()
    except:
        session.close()
        abort(
            406,
            "User with login {login} already exists".format(login=new_user.login),
        )
    session.close()

    return make_response(
        "User successfully created", 201
    )

def read_one(userid, token_info = None):
    """
    This function responds to a request for /api/users/{userid}
    with one matching user from users
    :param userid:        user id of user to find
    :param token_info:    dict with token info
    :return:              user matching user id
    """
    session = Session()
    try:
        user = session.query(User).filter(User.userid == userid).one()
    except:
        session.close()
        abort(
            404, "User with user id {userid} not found".format(userid=userid)
        )
    ret = {
        "userid": user.userid,
        "login": user.login,
        "fname": user.fname,
        "lname": user.lname,
        "role": user.role
    }
    session.close()
    return ret

def update(userid, body, token_info = None):
    """
    This function updates an existing user in the users structure
    :param userid:        user id of user to update in the users structure
    :param body:          user to update
    :param token_info:    dict with token info
    :return:              updated user structure
    """
    # Allowed people to update a user are either an admin or the currently logged in user
    isadmin = False
    scopes = token_info.get('scope')
    if scopes:
        if "admin" in scopes.split(' '):
            isadmin = True
    islogged = False
    if token_info.get('sub') == userid:
        islogged = True
    if not isadmin and not islogged:
        abort(
            400, "User can be modified by itself or by an admin only"
        )
    
    if "fname" in body:
        # Empty string is not allowed as a fname
        if not body["fname"]:
            abort(
                400, "Empty string is not allowed as a first name"
            )
    
    if "login" in body:
        # Empty string is not allowed as a login
        if not body["login"]:
            abort(
                400, "Empty string is not allowed as a login"
            )

    if "password" in body:
        # Empty string is not allowed as a password
        if not body["password"]:
            abort(
                400, "Empty string is not allowed as a password"
            )

    # Get User from DB
    session = Session()
    try:
        user = session.query(User).filter(User.userid == userid).one()
    except:
        session.close()
        abort(
            404, "User with user id {userid} not found".format(userid=userid)
        )

    user.login = body.get("login", user.login)
    user.fname = body.get("fname", user.fname)
    user.lname = body.get("lname", user.lname)
    if "password" in body:
        user.password = bcrypt.hashpw(body["password"].encode('utf-8'), bcrypt.gensalt(14)).decode('utf-8')
    user.role = body.get("role", user.role)

    try:
        session.commit()
    except:
        session.close()
        abort(
            400, "Failed to update user with id {userid}".format(userid=userid)
        )

    ret = {
        "userid": user.userid,
        "login": user.login,
        "fname": user.fname,
        "lname": user.lname,
        "role": user.role,
    }
    session.close()
    return ret

def delete(userid, token_info = None):
    """
    This function deletes a user from the users structure
    :param userid:        user id of user to delete
    :param token_info:    dict with token info
    :return:              200 on successful delete, 404 if not found
    """
    # Allowed people to delete a user are admin only
    isadmin = False
    scopes = token_info.get('scope')
    if scopes:
        if "admin" in scopes.split(' '):
            isadmin = True
    if not isadmin:
        abort(
            400, "Only admins are allowed to delete a user"
        )

    # Deleting the currently logged in user does not make sense
    islogged = False
    if token_info.get('sub') == userid:
        islogged = True
    if islogged:
        abort(
            400, "Deleting the currently logged in user is not possible"
        )

    # Does the user to delete exist?
    session = Session()
    try:
        user = session.query(User).filter(User.userid == userid).one()
    except:
        session.close()
        abort(
            404, "User with user id {userid} not found".format(userid=userid)
        )

    # Delete all actors related to this user
    for actor in session.query(Actor).filter(Actor.user_id == userid):
        session.delete(actor)

    # Set user_id to None for projects related to this user
    for project in session.query(Project).filter(Project.user_id == userid):
        project.user_id = None

    session.delete(user)
    try:
        session.commit()
    except:
        abort(
            400, "Failed to delete user"
        )

    session.close()
    
    return make_response(
        "{userid} successfully deleted".format(userid=userid), 200
    )
