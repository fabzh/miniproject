from flask import make_response, abort
from config import Session
from models import Base, User, Project, Task, Actor

def read_all(token_info = None):
    """
    This function responds to a request for /api/projects
    with the complete lists of projects
    :param token_info:    dict with token info
    :return:              sorted list of projects
    """
    ret = []
    session = Session()
    for project in session.query(Project).order_by(Project.projectid):
        ret.append({
            'projectid': project.projectid,
            'name': project.name,
            'description': project.description,
            'user_id': project.user_id,
            'state': project.state,
            'ctimestamp': project.ctimestamp
        })
    session.close()
    return ret

def create(body, token_info = None):
    """
    This function creates a new project in the projects structure
    based on the passed in project data
    :param body:          project to create in projects structure
    :param token_info:    dict with token info
    :return:              201 on success, 406 on project exists
    """
    new_project = Project(
        name = body.get("name", ""),
        description = body.get("description", ""),
        user_id = body.get("user_id", None),
        state = body.get("state", "In progress"),
    )

    if not new_project.name:
        abort(
            400,
            "Name is mandatory.",
        )

    session = Session()
    # If not userid is None, it has to exist in users list
    if not new_project.user_id is None:
        try:
            session.query(User).filter(User.userid == new_project.user_id).one()
        except:
            session.close()
            abort(
                404, "User with user id {userid} not found".format(userid=new_project.user_id)
            )

    session.add(new_project)
    try:
        session.commit()
    except:
        session.close()
        abort(
            400,
            "Failed to create project",
        )

    ret = {
        "projectid": new_project.projectid,
        "name": new_project.name,
        "description": new_project.description,
        "user_id": new_project.user_id,
        "ctimestamp": new_project.ctimestamp,
        "state": new_project.state
    }
    session.close()
    return make_response(
        ret, 201
    )

def read_one(projectid, token_info = None):
    """
    This function responds to a request for /api/projects/{projectid}
    with one matching project from projects
    :param projectid:     project id of project to find
    :param token_info:    dict with token info
    :return:              project matching project id
    """
    session = Session()
    try:
        project = session.query(Project).filter(Project.userid == projectid).one()
    except:
        session.close()
        abort(
            404, "Project with project id {projectid} not found".format(projectid=projectid)
        )
    
    ret = {
        "projectid": project.projectid,
        "name": project.name,
        "description": project.description,
        "user_id": project.user_id,
        "ctimestamp": project.ctimestamp,
        "state": project.state
    }
    session.close()
    return ret

def update(projectid, body, token_info = None):
    """
    This function updates an existing project in the projects structure
    :param projectid:     project id of project to update in the projects structure
    :param body:          project to update
    :param token_info:    dict with token info
    :return:              updated project structure
    """

    user_id = body.get("user_id", None)
    session = Session()
    # If user_id is not None, it has to exist in users list
    if not user_id is None:
        try:
            session.query(User).filter(User.userid == user_id).one()
        except:
            session.close()
            abort(
                404, "User with user id {userid} not found".format(userid=user_id)
            )

    try:
        project = session.query(Project).filter(Project.projectid == projectid).one()
    except:
        session.close()
        abort(
            404, "Project with project id {projectid} not found".format(projectid=projectid)
        )

    project.name = body.get("name", project.name)
    project.description = body.get("description", project.description)
    project.user_id = body.get("user_id", project.user_id)
    project.state = body.get("state", project.state)

    try:
        session.commit()
    except:
        session.close()
        abort(
            400, "Failed to update project with id {projectid}".format(projectid=projectid)
        )
    ret = {
        "projectid": project.projectid,
        "name": project.name,
        "description": project.description,
        "user_id": project.user_id,
        "state": project.state,
        "ctimestamp": project.ctimestamp
    }
    session.close()
    return ret

def delete(projectid, token_info = None):
    """
    This function deletes a project from the projects structure
    :param projectid:     project id of project to delete
    :param token_info:    dict with token info
    :return:              200 on successful delete, 404 if not found
    """

    session = Session()
    try:
        project = session.query(Project).filter(Project.projectid == projectid).one()
    except:
        abort(
            404, "Project with project id {projectid} not found".format(projectid=projectid)
        )
    # Delete all actors related to this project
    for actor in session.query(Actor).filter(Actor.project_id == projectid):
        session.delete(actor)

    # Delete all tasks related to this project
    for task in session.query(Task).filter(Task.project_id == projectid):
        session.delete(task)

    session.delete(project)
    try:
        session.commit()
    except:
        session.close()
        abort(
            400, "Failed to delete project with id {projectid}".format(projectid=projectid)
        )
    session.close()
    return make_response(
        "{projectid} successfully deleted".format(projectid=projectid), 200
    )
