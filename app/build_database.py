import os
import bcrypt
from config import engine, Session, DBFILE
from models import Base, User, Project, Task, Actor
import random

# Delete database file if it exists currently
if os.path.exists(DBFILE):
    secret = "{:04}".format(random.randint(0,9999))
    confirmation = input("DB file already exists.\nPlease enter the following code to confirm DB file removal : {}\n".format(secret))
    if confirmation == secret:
        os.remove(DBFILE)
    else:
        exit(1)

# Create the database
Base.metadata.create_all(engine)

# Create session 
session = Session()

# Create default user
user = User(
        login = 'admin',
        fname = 'admin',
        password = bcrypt.hashpw(b'admin',bcrypt.gensalt(14)).decode('utf-8'),
        role = 'admin'
    )
session.add(user)

session.commit()